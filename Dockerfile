FROM ubuntu:20.04

ARG GRAALVM_VERSION=21.3.1
ARG JAVA_VERSION=java11
ARG GRAALVM_SHA_AMD64=691609e34670bc0bb0ac00b2277bac2eff794a937b1bd1d48fe80e7064407142
ARG GRAALVM_SHA_ARM64=a06abbdfce44edf10ffa78a808dfa96c073c9fac115d306fe33604cc277aa1ca
ARG GRAALVM_PKG=https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-$GRAALVM_VERSION/graalvm-ce-$JAVA_VERSION-GRAALVM_ARCH-$GRAALVM_VERSION.tar.gz
ARG TARGETPLATFORM

ARG MAVEN_VERSION=3.8.4
ARG MAVEN_SHA=a9b2d825eacf2e771ed5d6b0e01398589ac1bfa4171f36154d1b5787879605507802f699da6f7cfc80732a5282fd31b28e4cd6052338cbef0fa1358b48a5e3c8
ARG MAVEN_PKG=https://downloads.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries

# Install base dependencies
RUN apt-get update \
    && apt-get install -y \
        autoconf \
        curl \
        tar \
        parallel \
    && rm -rf /var/lib/apt/lists/*

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Install GraalVM
ENV JAVA_HOME=/opt/graalvm-ce

ADD gu-wrapper.sh /usr/local/bin/gu
RUN set -eux \
    && mkdir -p /opt/graalvm-ce \
    && if [ "$TARGETPLATFORM" == "linux/amd64" ]; then GRAALVM_PKG=${GRAALVM_PKG/GRAALVM_ARCH/linux-amd64}; fi \
    && if [ "$TARGETPLATFORM" == "linux/arm64" ]; then GRAALVM_PKG=${GRAALVM_PKG/GRAALVM_ARCH/linux-aarch64}; fi \
    && curl -fsSL -o /tmp/graalvm-ce.tar.gz ${GRAALVM_PKG} \
    && if [ "$TARGETPLATFORM" == "linux/amd64" ]; then echo "${GRAALVM_SHA_AMD64}  /tmp/graalvm-ce.tar.gz" | sha256sum -c -; fi \
    && if [ "$TARGETPLATFORM" == "linux/arm64" ]; then echo "${GRAALVM_SHA_ARM64}  /tmp/graalvm-ce.tar.gz" | sha256sum -c -; fi \
    && tar -xzf /tmp/graalvm-ce.tar.gz -C /opt/graalvm-ce --strip-components=1 \
    && rm -f /tmp/graalvm-ce.tar.gz

RUN set -eux \
    && mkdir -p "/usr/java" \
    && ln -sfT "$JAVA_HOME" /usr/java/default \
    && ln -sfT "$JAVA_HOME" /usr/java/latest \
    && for bin in "$JAVA_HOME/bin/"*; do \
    base="$(basename "$bin")"; \
    [ ! -e "/usr/bin/$base" ]; \
    update-alternatives --install "/usr/bin/$base" "$base" "$bin" 20000; \
    done \
    && chmod +x /usr/local/bin/gu

RUN java -version

# Install GraalVM utilities
RUN /usr/local/bin/gu install native-image

# Install Maven
RUN set -eux \
  && mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${MAVEN_PKG}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${MAVEN_SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME=/usr/share/maven
ENV MAVEN_CONFIG=/root/.m2

RUN mvn --version

# Default to UTF-8 file.encoding
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    LANGUAGE=C.UTF-8

# Create dirs and users
RUN mkdir -p /opt/graalvm/agent/build \
    && sed -i '/[ -z \"PS1\" ] && return/a\\ncase $- in\n*i*) ;;\n*) return;;\nesac' /root/.bashrc \
    && useradd --create-home --shell /bin/bash --uid 1000 pipelines

WORKDIR /opt/graalvm/agent/build

CMD ["mvn"]